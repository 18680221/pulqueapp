import 'dart:convert';
import 'dart:ffi';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class Detalles extends StatefulWidget {
  final id_negocio;
  const Detalles({this.id_negocio, Key? key}) : super(key: key);

  @override
  State<Detalles> createState() => _Detalles();

  // nombre nuestra classe

}

class _Detalles extends State<Detalles> {
  List<dynamic> pulques = [];
  List<dynamic> comentarios = [];

  bool cargando = true;
  double promedio = 0.0;
  final comentario = TextEditingController();
  @override
  void initState() {
    super.initState();
    Search();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Negocios"),
        ),
        body: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Center(
              child: cargando == true
                  ? CupertinoActivityIndicator(
                      radius: 20,
                    )
                  : Stack(
                      children: [
                        Opacity(
                          child: Image.asset(
                            'assets/fondos.png',
                          ),
                          opacity: 0.6,
                        ),
                        Container(
                          child: ListView(children: [
                            Text(
                              "Nombre del negocio",
                              style: TextStyle(
                                fontFamily: "aAvocadoTaco",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              pulques[0]["negocio"],
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Ubucacion",
                              style: TextStyle(
                                fontFamily: "aAvocadoTaco",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              pulques[0]["ubicacion"],
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Contacto",
                              style: TextStyle(
                                fontFamily: "aAvocadoTaco",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              pulques[0]["correo"],
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                            Text(
                              pulques[0]["telefono"],
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Calificacion",
                              style: TextStyle(
                                fontFamily: "aAvocadoTaco",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            RatingBarIndicator(
                              rating: promedio,
                              itemBuilder: (context, index) => Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              itemCount: 5,
                              itemSize: 50.0,
                              direction: Axis.horizontal,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Menu",
                              style: TextStyle(
                                fontFamily: "aAvocadoTaco",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * .30,
                              child: ListView.builder(
                                  itemCount: pulques[0]["productos"].length,
                                  itemBuilder: (BuildContext context, int j) {
                                    return Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Colors.black),
                                            ),
                                            child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.stretch,
                                                children: [
                                                  Text(
                                                    "Producto",
                                                    style: TextStyle(
                                                      fontFamily:
                                                          "aAvocadoTaco",
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  Text(
                                                    pulques[0]["productos"][j]
                                                        ["sabor"],
                                                    style: TextStyle(
                                                      fontSize: 20,
                                                    ),
                                                  ),
                                                  Text(
                                                    "Cantidad",
                                                    style: TextStyle(
                                                      fontFamily:
                                                          "aAvocadoTaco",
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  Text(
                                                    pulques[0]["productos"][j]
                                                        ["cantidad"],
                                                    style: TextStyle(
                                                      fontSize: 20,
                                                    ),
                                                  ),
                                                  Text(
                                                    "Precio",
                                                    style: TextStyle(
                                                      fontFamily:
                                                          "aAvocadoTaco",
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  Text(
                                                    pulques[0]["productos"][j]
                                                        ["precio"],
                                                    style: TextStyle(
                                                      fontSize: 20,
                                                    ),
                                                  ),
                                                ]),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          )
                                        ],
                                      ),
                                    );
                                  }),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Comentarios",
                              style: TextStyle(
                                fontFamily: "aAvocadoTaco",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "Escribe un comentario",
                              style: TextStyle(fontSize: 20),
                            ),
                            TextField(
                              controller: comentario,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: RatingBar.builder(
                                initialRating: 0,
                                minRating: 0,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 4.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) async {
                                  print(rating);
                                  int numero = 0;
                                  double suma = 0;
                                  if (promedio == 0) {
                                    numero = 1;
                                    suma = rating;
                                  } else {
                                    numero = int.parse(pulques[0]["numero"]);
                                    suma = double.parse(pulques[0]["suma"]);
                                    suma += rating;
                                    numero += 1;
                                  }

                                  var data = json.encode(<String, dynamic>{
                                    "_id": pulques[0]["_id"],
                                    "suma": suma.toString(),
                                  });
                                  await updatesuma(data);
                                  print("suma");
                                  var d = json.encode(<String, dynamic>{
                                    "_id": pulques[0]["_id"],
                                    "numero": numero.toString(),
                                  });
                                  await updatenumero(d);
                                  print("numero");

                                  setState(() {
                                    promedio = suma / numero;
                                  });
                                },
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            CupertinoButton(
                              onPressed: () {
                                String comen = comentario.text;
                                setState(() {
                                  comentarios.add(comen);
                                });
                                var data = json.encode(<String, dynamic>{
                                  "_id": pulques[0]["_id"],
                                  "comentarios": comentarios,
                                });
                                final respuesta = update(data);

                                ScaffoldMessenger.of(context)
                                    .showSnackBar(const SnackBar(
                                  content: Text("Comentarios agregado"),
                                  duration: Duration(seconds: 7),
                                  behavior: SnackBarBehavior.floating,
                                  padding: EdgeInsets.all(16.0),
                                ));
                              },
                              color: Color.fromARGB(255, 114, 164, 221),
                              child: Text("Agregar comentario"),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Comentarios",
                              style: TextStyle(
                                fontFamily: "aAvocadoTaco",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            comentarios.length > 0
                                ? SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        .60,
                                    child: ListView.builder(
                                      itemCount: comentarios.length,
                                      itemBuilder:
                                          (BuildContext context, int i) {
                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.stretch,
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.black),
                                              ),
                                              child: Text(
                                                comentarios[i],
                                                style: TextStyle(fontSize: 20),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 20,
                                            )
                                          ],
                                        );
                                      },
                                    ),
                                  )
                                : Text("")
                          ]),
                        )
                      ],
                    ),
            )));
  }

  Future<http.Response> update(json) {
    print(update);
    return http.post(
      Uri.parse('http://192.168.0.12:5000/update'),
      headers: {
        "Accept": "application/json",
        "Access-Control_Allow_Origin": "*",
        'Content-Type': 'application/json'
      },
      body: json,
    );
  }

  Future<http.Response> updatenumero(json) {
    return http.post(
      Uri.parse('http://192.168.0.12:5000/updatenumero'),
      headers: {
        "Accept": "application/json",
        "Access-Control_Allow_Origin": "*",
        'Content-Type': 'application/json'
      },
      body: json,
    );
  }

  Future<http.Response> updatesuma(json) {
    return http.post(
      Uri.parse('http://192.168.0.12:5000/updatesuma'),
      headers: {
        "Accept": "application/json",
        "Access-Control_Allow_Origin": "*",
        'Content-Type': 'application/json'
      },
      body: json,
    );
  }

  Future Search() async {
    final respuesta = await http.get(Uri.parse(
        'http://192.168.0.12:5000/get/' + widget.id_negocio.toString()));
    var jsonResponse = json.decode(respuesta.body) as Map<String, dynamic>;
    //final user = json.decode(respuesta.body);

    setState(() {
      pulques = jsonResponse["response"];
      cargando = false;
      if (pulques[0]["suma"] != "") {
        double suma = double.parse(pulques[0]["suma"]);
        int numero = int.parse(pulques[0]["numero"]);
        promedio = suma / numero;
      } else {
        print("No hay cali");
      }
      if (pulques[0]["comentarios"] != null) {
        comentarios = pulques[0]["comentarios"];
      } else {
        print("No hay comentarios");
      }
      print(pulques);
    });
  }
}
