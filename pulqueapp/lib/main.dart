import 'package:flutter/material.dart';
import 'package:pulqueapp/negocios.dart';
import 'package:pulqueapp/registro.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PulqueApp',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(32.0),
          child: Stack(
            children: [
              Image.asset(
                'assets/nube.png',
                width: MediaQuery.of(context).size.width,
              ),
              Container(
                height: 200,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(15.0),
                alignment: Alignment.bottomCenter,
                child: Text(
                  "PulqueApp",
                  style: TextStyle(
                      color: Color.fromARGB(255, 14, 50, 114),
                      fontSize: 35,
                      fontFamily: "aAvocadoTaco"),
                ),
              ),
            ],
          ),
        ),
        Image.asset(
          'assets/fondo.png',
          height: 300,
          width: MediaQuery.of(context).size.width,
        ),
        Container(
          child: Row(children: [
            SizedBox(
              width: 20,
            ),
            Container(
              height: 90,
              width: 90,
              child: IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Registro()),
                    );
                  },
                  icon: Image.asset(
                    'assets/storemas.png',
                  )),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.40,
            ),
            Container(
              height: 90,
              width: 90,
              child: IconButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Negocios()),
                    );
                  },
                  icon: Image.asset(
                    'assets/storeojo.png',
                  )),
            )
          ]),
        )
      ],
    )
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
