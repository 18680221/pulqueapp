import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:pulqueapp/detalles.dart';

class Negocios extends StatefulWidget {
  const Negocios({Key? key}) : super(key: key);

  @override
  State<Negocios> createState() => _Negocios();

  // nombre nuestra classe

}

class _Negocios extends State<Negocios> {
  List<dynamic> pulques = [];
  bool cargando = true;
  @override
  void initState() {
    super.initState();
    Search();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Negocios"),
        ),
        body: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Center(
              child: cargando == true
                  ? CupertinoActivityIndicator(
                      radius: 20,
                    )
                  : Stack(
                      children: [
                        Opacity(
                          child: Image.asset(
                            'assets/fondos.png',
                          ),
                          opacity: 0.6,
                        ),
                        Container(
                          child: ListView.builder(
                            itemCount: pulques.length,
                            itemBuilder: (BuildContext context, int i) {
                              // ignore: unnecessary_new
                              return new GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Detalles(
                                                id_negocio: i,
                                              )));
                                },
                                child: Column(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: [
                                          Text(
                                            "Nombre:",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            pulques[i]["negocio"],
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          Text(
                                            "Direccion:",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            pulques[i]["ubicacion"],
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            "Contacto",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            pulques[i]["correo"],
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            pulques[i]["telefono"],
                                            style: TextStyle(fontSize: 20),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 20,
                                    )
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
            )));
  }

  Future Search() async {
    final respuesta = await http.get(Uri.parse('http://192.168.0.12:5000/'));
    var jsonResponse = json.decode(respuesta.body) as Map<String, dynamic>;
    //final user = json.decode(respuesta.body);

    setState(() {
      pulques = jsonResponse["response"];
      cargando = false;
      print(pulques);
    });
  }
}
