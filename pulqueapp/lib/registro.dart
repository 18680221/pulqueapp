import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class Registro extends StatefulWidget {
  const Registro({Key? key}) : super(key: key);

  @override
  State<Registro> createState() => _Registro();

  // nombre nuestra classe

}

class _Registro extends State<Registro> {
  final formulario = GlobalKey<FormState>();
  final nombre = TextEditingController();
  final ubicacion = TextEditingController();
  final tipos = TextEditingController();
  final cantidad = TextEditingController();
  final precio = TextEditingController();
  final correo = TextEditingController();
  final telefono = TextEditingController();
  bool cargando = false;
  String listapulque = "";
  List<dynamic> pulques = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Registro")),
      body: ListView(
        children: [
          Stack(
            children: [
              Opacity(
                child: Image.asset(
                  'assets/fondos.png',
                ),
                opacity: 0.6,
              ),
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: Form(
                  key: formulario,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          "Nombre del establecimiento",
                          style: TextStyle(
                            fontFamily: "aAvocadoTaco",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextField(
                          controller: nombre,
                          onChanged: (text) {
                            print("First text field: $text");
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          "Ubicacion",
                          style: TextStyle(
                            fontFamily: "aAvocadoTaco",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextField(
                          controller: ubicacion,
                          onChanged: (text) {
                            print("First text field: $text");
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          "Menu",
                          style: TextStyle(
                              fontFamily: "aAvocadoTaco",
                              fontWeight: FontWeight.bold,
                              fontSize: 35),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          "Tipo de pulque (Sabor)",
                          style: TextStyle(
                            fontFamily: "aAvocadoTaco",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextField(
                          controller: tipos,
                          onChanged: (text) {
                            print("First text field: $text");
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          "Cantidad (ml, L) ",
                          style: TextStyle(
                            fontFamily: "aAvocadoTaco",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextField(
                          controller: cantidad,
                          onChanged: (text) {
                            print("First text field: $text");
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          "Precio",
                          style: TextStyle(
                            fontFamily: "aAvocadoTaco",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextField(
                          controller: precio,
                          onChanged: (text) {
                            print("First text field: $text");
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        CupertinoButton(
                            color: Color.fromARGB(255, 18, 44, 73),
                            onPressed: () {
                              setState(() {
                                pulques.add({
                                  "sabor": tipos.text,
                                  "cantidad": cantidad.text,
                                  "precio": precio.text
                                });
                              });
                            },
                            child: Text("Agregar")),
                        SizedBox(
                          height: 30,
                        ),
                        pulques.length == 0
                            ? Text("")
                            : SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * .20,
                                child: ListView.builder(
                                    itemCount: pulques.length,
                                    itemBuilder: (BuildContext context, int i) {
                                      return new Container(
                                        child: Row(
                                          children: [
                                            Text(pulques[i]["sabor"]),
                                            Text(pulques[i]["cantidad"]),
                                            Text(pulques[i]["precio"]),
                                            CupertinoButton(
                                                child: Text("Eliminar"),
                                                onPressed: () {
                                                  setState(() {
                                                    pulques.removeAt(i);
                                                  });
                                                })
                                          ],
                                        ),
                                      );
                                    })),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          "Correo",
                          style: TextStyle(
                            fontFamily: "aAvocadoTaco",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextField(
                          controller: correo,
                          onChanged: (text) {
                            print("First text field: $text");
                          },
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          "Telefono",
                          style: TextStyle(
                            fontFamily: "aAvocadoTaco",
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextField(
                          controller: telefono,
                          onChanged: (text) {
                            print("First text field: $text");
                          },
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        pulques.length > 0
                            ? CupertinoButton(
                                color: Color.fromARGB(255, 85, 132, 183),
                                onPressed: () {
                                  setState(() {
                                    cargando = true;
                                  });
                                  String name = nombre.text;
                                  String location = ubicacion.text;
                                  String mail = correo.text;

                                  String phone = telefono.text;
                                  var data = json.encode(<String, dynamic>{
                                    "negocio": name,
                                    "ubicacion": location,
                                    "productos": pulques,
                                    "correo": mail,
                                    "telefono": phone
                                  });
                                  final respuesta = add(data);
                                  setState(() {
                                    cargando = false;
                                  });
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(const SnackBar(
                                    content: Text("Negocio agregado"),
                                    duration: Duration(seconds: 7),
                                    behavior: SnackBarBehavior.floating,
                                    padding: EdgeInsets.all(16.0),
                                  ));
                                },
                                child: Text("Registrar"))
                            : Text(""),
                        SizedBox(
                          height: 20,
                        ),
                        cargando == true
                            ? Center(
                                child: CupertinoActivityIndicator(
                                radius: 20,
                              ))
                            : Text(""),
                      ]),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<http.Response> add(json) {
    return http.post(
      Uri.parse('http://192.168.0.12:5000/add'),
      headers: {
        "Accept": "application/json",
        "Access-Control_Allow_Origin": "*",
        'Content-Type': 'application/json'
      },
      body: json,
    );
  }
}
